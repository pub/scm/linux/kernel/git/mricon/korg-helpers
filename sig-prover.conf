[DEFAULT]
# Each section is weighted -- the higher the number, the more likely
# it is to be picked during each random run
weight = 10
# Allows us to fake a user-agent in case the remote is returning different results
# based on the name of sig-prover. You can have multiple entries -- a random one
# will be chosen. By default, we use User-Agent: sig-prover/{version}
#useragent = Wget/1.21.1 (linux-gnu)
#            curl/7.54.1
# The directory with all keyrings. To generate a keyring, run:
# gpg --no-default-keyring --keyring=./foo.gpg --import key1.asc key2.asc ...
keyringdir = ./sig-prover-keyrings
# We download and uncompress each tarball, so make sure you have enough room
# in the temporary directory location.
#tempdir = /tmp
# If you set notify, then an email will be sent out to this address instead of just
# printing an error message. I suggest if you want to help us out, first few runs
# make sure that things are properly verifying, and then set up to notify
# admin@kernel.org on any errors (and cc yourself, if you like)
#notify = admin@kernel.org
#notify_cc = you@some.addr
# Please set mailfrom if you've set "notify", as we may need to talk to you.
#mailfrom = you@some.addr
# you can use any modern authenticated SMTP host by setting the values below,
# or you can just set mailhost = localhost and leave the rest commented out
#mailhost = mail.kernel.org:587
#mailtls = yes
#mailuser = [some username]
#mailpass = [some password]
# These hosts will be concatenated with the paths defined in each section.
# You can reduce this to just the nearest host to you.
hosts = https://na.edge.kernel.org
        https://eu.edge.kernel.org
        https://ap.edge.kernel.org
# We grab sha256sums.asc from each path defined in the sections below, which
# is inline-signed by a special "autosigner" key. This key should NOT be added
# to the other keyrings, as it is NOT supposed to be signing any releases
# (just the checksums). This is VERY important.
dirsigner_keyring = dirsigner.gpg
# When we find an entry that ends with .tar.foo, we will look for a matching
# unfoo entry. Check that bunzip2 is actually available on your system, as it may
# no longer be installed by default, but old releases still need it.
unxz = /usr/bin/unxz
unbz2 = /usr/bin/bunzip2
ungz = /usr/bin/gunzip
# You can override gpg binary location if it's not /usr/bin/gpg
#gpgbin = /usr/bin/gpg2
# If you don't define this value, we'll exit after the first run.
sleep = 60

# You can override any of the DEFAULT values in the sections below, and anything
# defined in DEFAULT is defined in each section.
[latest]
weight = 20
# If we find a "json" entry, we'll use it instead of using hosts + paths + masks
json = https://www.kernel.org/releases.json
keyring = kernel.gpg

[current-kernels]
weight = 50
# We append these to "hosts"
# The starting and trailing slashes are important!
paths = /pub/linux/kernel/v4.x/
        /pub/linux/kernel/v5.x/
# this is a regex that defines which files from sha256sums.asc we'll consider
masks = linux-\d.*\.tar\..*
keyring = kernel.gpg

[git]
weight = 10
paths = /pub/software/scm/git/
masks = git-\d.*\.tar\..*
keyring = git.gpg

# If you have bandwidth to spare, please check these, too.
#[old-kernels]
#weight = 5
#paths = /pub/linux/kernel/v3.x/
#        /pub/linux/kernel/v2.6/
#        /pub/linux/kernel/v2.6/longterm/
#        /pub/linux/kernel/v2.6/longterm/v2.6.27/
#        /pub/linux/kernel/v2.6/longterm/v2.6.32/
#        /pub/linux/kernel/v2.6/longterm/v2.6.33/
#        /pub/linux/kernel/v2.6/longterm/v2.6.34/
#        /pub/linux/kernel/v2.6/longterm/v2.6.35/
#masks = linux-\d.*\.tar\..*
#keyring = old-kernel.gpg
